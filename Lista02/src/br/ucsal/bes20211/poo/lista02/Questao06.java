package br.ucsal.bes20211.poo.lista02;

import java.util.Scanner;

public class Questao06 {

	static Scanner sc = new Scanner(System.in);
	public static void main(String[] args) {
		
		resultado();
	}
	private static void resultado() {
		String nome,sobrenome;
		nome = obterNome();
		sobrenome = obterSobrenome();
		
		String nomeCompleto = concatenarNomeCompleto(nome, sobrenome);
		
		exibirNome(nomeCompleto);
	}
	private static void exibirNome(String nomeCompleto) {
		System.out.println("O nome completo �: "+ nomeCompleto);
	}
	private static String concatenarNomeCompleto(String nome, String sobrenome) {
		String nomeCompleto = nome+" "+sobrenome;
		return nomeCompleto;
	}
	private static String obterSobrenome() {
		String sobrenome;
		System.out.print("Informe o sobrenome: ");
		sobrenome=sc.nextLine();
		return sobrenome;
	}
	private static String obterNome() {
		String nome;
		System.out.print("Informe o nome: ");
		nome=sc.nextLine();
		return nome;
	}

}
