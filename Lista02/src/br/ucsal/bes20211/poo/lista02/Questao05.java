package br.ucsal.bes20211.poo.lista02;

import java.util.Scanner;

public class Questao05 {

	static Scanner sc = new Scanner(System.in);

	public static void main(String[] args) {

		resultado();

	}

	private static void resultado() {
		double area, base, altura;
		
		base = captarBase();
		altura = captarAltura();
		
		area = calcularArea(base, altura);
		exibirArea(area);
	}

	private static void exibirArea(double area) {
		System.out.println(area);
	}

	private static double calcularArea(double base, double altura) {
		double area;
		area = (base*altura)/2;
		return area;
	}

	private static double captarAltura() {
		double altura;
		System.out.print("Informe o valor da altura: ");
		altura=sc.nextDouble();
		return altura;
	}

	private static double captarBase() {
		double base;
		System.out.print("Informe o valor da base: ");
		base=sc.nextDouble();
		return base;
	}

}
