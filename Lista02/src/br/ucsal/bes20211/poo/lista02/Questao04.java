package br.ucsal.bes20211.poo.lista02;

import java.util.Scanner;

public class Questao04 {

	static Scanner sc = new Scanner(System.in);
	public static void main(String[] args) {
		
		resultado();
	
	}
	private static void resultado() {
		final double pi = 3.14159;
		double area, raio;
		
		raio = captarValorRaio();
		
		area = calcularArea(pi, raio);
		exibirArea(area);
	}
	private static void exibirArea(double area) {
		System.out.println(area);
	}
	private static double calcularArea(final double pi, double raio) {
		double area;
		area= pi * Math.pow(raio, 2);
		return area;
	}
	private static double captarValorRaio() {
		double raio;
		System.out.print("Informe o valor do raio: ");
		raio=sc.nextDouble();
		return raio;
	}

}
