package application;

import methods.Numbers;

public class Main {

	public static void main(String[] args) {
		
		executor();

	}

	private static void executor() {
		int[] vet = new int[5];
		Numbers numbers = new Numbers();
		
		numbers.obterNumeros(vet);
		numbers.encontrarMaiorNumero(vet);
		numbers.exibirMaiorNumero(vet);
	}

}
