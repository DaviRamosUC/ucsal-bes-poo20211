package methods;

import java.util.Scanner;

public class Numbers {

	public void obterNumeros(int[] vet) {
		Scanner sc = new Scanner(System.in);
		System.out.print("Informe 5 n�meros: ");
		for (int i = 0; i < vet.length; i++) {
			vet[i] = sc.nextInt();
		}
		sc.close();
	}

	public int encontrarMaiorNumero(int[] vet) {
		int maior = 0;
		for (int i = 0; i < vet.length; i++) {
			maior = (vet[i]>maior) ? vet[i] : maior; 
		}
		return maior;
	}

	public void exibirMaiorNumero(int[] vet) {
		System.out.println("Dentre os n�meros informados, o maior foi: "+encontrarMaiorNumero(vet));
	}

}
