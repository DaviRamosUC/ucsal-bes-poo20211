package br.ucsal.bes20211.poo.lista01;

import java.util.Scanner;

public class Quest�o03 {

	static Scanner sc = new Scanner(System.in);

	public static void main(String[] args) {
		resultado();
	}

	private static void resultado() {
		int[] lista = receberDados();		
		exibeCalculos(lista);
	}

	private static void exibeCalculos(int[] lista) {
		System.out.println("A m�dia dos n�meros �: "+ calculaMedia(lista));
		System.out.println("O maior dos n�meros �: "+ calculaMaior(lista));
		System.out.println("O menor dos n�meros �: "+ calculaMenor(lista));
	}

	private static int calculaMaior(int[] lista) {
		int ma=0;
		for (int i = 0; i < lista.length; i++) {
			ma = lista[0];
			if (ma < lista[i]) {
				ma = lista[i];
			}
		}
		return ma;
	}

	private static int calculaMenor(int[] lista) {
		int me=0;
		for (int i = 0; i < lista.length; i++) {
			me = lista[0];
			if (me > lista[i]) {
				me = lista[i];
			}
		}
		return me;
	}

	private static double calculaMedia(int[] lista) {
		double media = 0;
		for (int i = 0; i < lista.length; i++) {
			media += lista[i];
		}
		return media / lista.length;
	}

	private static int[] receberDados() {
		System.out.println("Informe 10 n�meros inteiros");
		int[] lista = new int[10];

		for (int i = 0; i < lista.length; i++) {
			System.out.println("Informe o " + (i+1) + "� n�mero");
			lista[i] = sc.nextInt();
		}
		return lista;
	}

}
