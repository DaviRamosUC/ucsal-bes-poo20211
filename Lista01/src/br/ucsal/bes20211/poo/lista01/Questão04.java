package br.ucsal.bes20211.poo.lista01;

import java.util.Scanner;

public class Quest�o04 {

	static Scanner sc = new Scanner(System.in);

	public static void main(String[] args) {

		String[] nome = new String[10];
		double peso, altura, mPeso = 0, mAltura = 0;
		int iPeso = 0, iAltura = 0;

		System.out.println("Informe o nome peso e altura de 10 pessoas");
		for (int i = 0; i < nome.length; i++) {
			System.out.println("Informe o nome peso e altura da " + (i + 1) + " pessoas");
			
			nome[i] = sc.nextLine();
			peso = sc.nextDouble();
			altura = sc.nextDouble();
			sc.nextLine();
			
			if (peso > mPeso) {
				mPeso = peso;
				iPeso = i;
			}
			if (altura > mAltura) {
				mAltura = altura;
				iAltura = i;
			}
		}

		exibePesoEAltura(nome, mPeso, mAltura, iPeso, iAltura);
	}

	private static void exibePesoEAltura(String[] nome, double mPeso, double mAltura, int iPeso, int iAltura) {
		System.out.println("A pessoa mais pesada �: " + nome[iPeso] + " com peso: " + mPeso);
		System.out.println("A pessoa mais alta �: " + nome[iAltura] + " com altura: " + mAltura);
	}

}
