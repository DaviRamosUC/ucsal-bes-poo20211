package br.ucsal.bes20211.poo.lista01;

import java.util.Scanner;

public class Questao01 {

	static Scanner sc = new Scanner(System.in);
	
	public static void main(String[] args) {
		
		exibirCalculoEInformacoes();

	}
	
	private  static void exibirCalculoEInformacoes() {
		int nota;
		String conceito;
		
		nota = obterNota(sc);
		conceito=calcularConceito(nota);
		exibirConceito(conceito, nota);
	}
	
	private  static int obterNota(Scanner sc) {
		System.out.print("Por favor informe a nota: ");
		return  sc.nextInt();
	}
	
	private static String calcularConceito(int nota) {
		String conceito=null;
		if (nota<=49) {
			conceito = "Insuficiente";
		}else if(nota<=64) {
			conceito = "Regular";
		}else if(nota<=84) {
			conceito = "Bom";
		}else if(nota<=100) {
			conceito = "�timo";
		}
		return conceito;
	}
	
	private static void exibirConceito(String conceito, int nota){
		System.out.println("O conceito para a nota " + nota + " �: " + conceito + ".");
	}
	
	

}
