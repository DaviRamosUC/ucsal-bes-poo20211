package br.ucsal.bes20211.poo.lista01;

import java.util.Scanner;

public class Questao05 {
	
	static Scanner sc = new Scanner(System.in);
	public static void main(String[] args) {
		int n,a1,r;
		System.out.print("Informe a quantidade de termos a serem apresentados: ");
		n = sc.nextInt();
		System.out.print("Informe o primeiro termo: ");
		a1 = sc.nextInt();
		System.out.print("Informe a raz�o: ");
		r=sc.nextInt();
		
		resultado(n, a1, r);
		
	}
	private static void resultado(int n, int a1, int r) {
		int[] lista = leituraDeDados(n, a1, r);
		
		int soma = somaDosTermos(lista);
		
		exibirListaESoma(lista, soma);
	}
	
	private static void exibirListaESoma(int[] lista, int soma) {
		//Exibe lista
		System.out.print("Itens da lista: ");
		for (int i = 0; i < lista.length; i++) {
			System.out.print(lista[i]+" ");
		}
		//Exibe soma
		System.out.println("Soma total: "+soma);
	}
	private static int somaDosTermos(int[] lista) {
		//Soma de todos os termos
		int soma=0;
		for (int i = 0; i < lista.length; i++) {
			soma+=lista[i];
		}
		return soma;
	}
	private static int[] leituraDeDados(int n, int a1, int r) {
		int[] lista = new int[n];
		lista[0] = a1;
		
		//Leitura dos n�meros
		for (int i = 1; i < n; i++) {
			lista[i] = lista[i-1] + r; 
		}
		return lista;
	}

}
