package br.ucsal.bes20211.poo.lista01;

import java.util.Scanner;

public class Questao02 {

	static Scanner sc = new Scanner(System.in);

	public static void main(String[] args) {

		resultado();


	}
	
	private static void resultado() {
		int n = obterNumero();
		double E = obterExpressaoE(n);
		imprimirResultado(n, E);
	}

	private static void imprimirResultado(int n, double E) {
		System.out.println("O resultado para a express�o E de "+n+" �: "+E);
	}

	private static double obterExpressaoE(int n) {
		double E = 0;

		for (int i = 0; i <= n; i++) {
			E += 1 /(double)fatorial(i);
		}
		return E;
	}

	private static int obterNumero() {
		System.out.print("Informe um valor: ");
		return sc.nextInt();
	}

	private static int fatorial(int n) {
		int r = n, cont=n;

		for (int i = 0; i <= cont; i++) {
			n=(n==1) ? n+1 : n;
			r *= --n;
		}
		return (r==0) ? 1 : r;
	}

}
