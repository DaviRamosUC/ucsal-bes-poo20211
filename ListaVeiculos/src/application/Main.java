package application;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;

import model.entities.Proprietario;
import model.entities.Veiculo;

public class Main {

	public static void main(String[] args) {
		
		try {
			executor();
		} catch (ParseException e) {
			System.out.println(e.getMessage());
		}

	}

	private static void executor() throws ParseException {
		Scanner sc = new Scanner(System.in);
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy");
		
		System.out.print("Infome o CPF do propriet�rio: ");
		String cpf = sc.nextLine();
		System.out.print("Infome o nome do propriet�rio: ");
		String nome = sc.nextLine();
		System.out.println("Infome o endereco do propriet�rio contendo: � CEP � Logradouro � N�mero � Rua � Bairro");
		String[] endereco = new String[5];
		for (int i = 0; i < endereco.length; i++) {
			endereco[i] = sc.nextLine();
		}
		System.out.print("Infome o telefone do propriet�rio: ");
		String telefone = sc.nextLine();
		
		Proprietario p = new Proprietario(cpf, nome, endereco, telefone);
		
		boolean saida = false;
		do {
			System.out.println("Sobre o ve�culo: ");
			System.out.print("Informe a placa: ");
			String placa = sc.nextLine();
			System.out.print("Informe o ano de fabrica��o (yyyy): ");
			Date anoFabricacao = sdf.parse(sc.nextLine());
			System.out.print("Informe o valor do veiculo: ");
			double valor = sc.nextDouble();
			
			Veiculo v = new Veiculo(placa, anoFabricacao, valor, p);
			
			p.addVeiculos(v);
			
			System.out.print("Deseja cadastrar outro ve�culo? s/n: ");
			sc.nextLine();
			char resposta = sc.nextLine().charAt(0);
			saida = (resposta == 's') ? true : false;
			
		} while (saida == true);
		
		System.out.println("\n"+p.toString());
		
		sc.close();
	}

}
