package model.entities;

import java.text.SimpleDateFormat;
import java.util.Date;

public class Veiculo {
	
	private String placa;
	private Date anoFabricacao;
	private double valor;
	private Proprietario proprietario;
	
	public Veiculo() {
	
	}

	public Veiculo(String placa, Date anoFabricacao, double valor, Proprietario proprietario) {
		this.placa = placa;
		this.anoFabricacao = anoFabricacao;
		this.valor = valor;
		this.proprietario = proprietario;
	}

	public String getPlaca() {
		return placa;
	}

	public void setPlaca(String placa) {
		this.placa = placa;
	}

	public Date getAnoFabricacao() {
		return anoFabricacao;
	}

	public void setAnoFabricacao(Date anoFabricacao) {
		this.anoFabricacao = anoFabricacao;
	}

	public double getValor() {
		return valor;
	}

	public void setValor(double valor) {
		this.valor = valor;
	}

	public Proprietario getProprietario() {
		return proprietario;
	}

	public void setProprietario(Proprietario proprietario) {
		this.proprietario = proprietario;
	}

	@Override
	public String toString() {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy");
		StringBuilder sb = new StringBuilder();
		sb.append("Dados do Ve�culo: ");
		sb.append("\nPlaca: "+placa);
		sb.append("\nAno de Fabrica��o: "+sdf.format(anoFabricacao));
		sb.append("\nValor: "+valor);
		sb.append("\n");
		
		return sb.toString();
	}
	
	
	
	
	
}
