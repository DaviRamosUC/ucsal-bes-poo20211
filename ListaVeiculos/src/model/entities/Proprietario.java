package model.entities;

import java.util.ArrayList;
import java.util.List;

public class Proprietario {
	
	private String cpf;
	private String nome;
	private String[] endereco = new String[5];
	private String telefone;
	List<Veiculo> veiculos= new ArrayList<>();
	
	public Proprietario() {
		
	}

	public Proprietario(String cpf, String nome, String[] endereco, String telefone) {
		this.cpf = cpf;
		this.nome = nome;
		this.endereco = endereco;
		this.telefone = telefone;
	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String[] getEndereco() {
		return endereco;
	}

	public void setEndereco(String[] endereco) {
		this.endereco = endereco;
	}

	public String getTelefone() {
		return telefone;
	}

	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}

	public void removeVeiculos(Veiculo v) {
		veiculos.remove(v);
	}

	public void addVeiculos(Veiculo v) {
		veiculos.add(v);
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		
		sb.append("Dados do Propriet�rio: ");
		sb.append("\nCPF: "+cpf);
		sb.append("\nNome: "+nome);
		sb.append("\nCEP: "+endereco[0]);
		sb.append("\nLogradouro: "+endereco[1]);
		sb.append("\nN�mero: "+endereco[2]);
		sb.append("\nRua: "+endereco[3]);
		sb.append("\nBairro: "+endereco[4]);
		sb.append("\nTelefone: "+telefone);
		
		for (Veiculo v : veiculos) {
			System.out.println(v.toString());
		}
		
		
		return sb.toString();
	}
	
	
	
	
	
	
	
	
	
	
}
